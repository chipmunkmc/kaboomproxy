const fs = require("fs");
const config = require("../config.js");
const commands = fs.readdirSync("./commands");
module.exports = function(vanillaclient, client, server, data) {
    let msg = data.message;
    if(msg.startsWith(config.proxy.prefix)) {
        let args = msg.split(" ").slice(1);
        let cmd = msg.split(" ")[0].substr(1);
        if(!commands.includes(`${cmd}.js`)) return "cancel";
        let cmdfile = require(`../commands/${cmd}.js`); 
        cmdfile.exec(vanillaclient, client, server, args);
        return "cancel";
    }
};