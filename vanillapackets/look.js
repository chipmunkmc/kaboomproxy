module.exports = function(vanillaclient, client, server, data) {
    client.position.yaw = data.yaw;
    client.position.pitch = data.pitch;
};