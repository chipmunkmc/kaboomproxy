const UUID = require('uuid').v4();

function createBossbar(vanillaclient, name) {
    vanillaclient.write('boss_bar', {
        entityUUID: UUID,
        action: 0,
        title: typeof name === "string" ? JSON.stringify({ "text": name }) : JSON.stringify(name),
        health: 0,
        color: 6,
        dividers: 0,
        flags: 0
    });
}

function changeName(vanillaclient, name) {
    vanillaclient.write('boss_bar', {
        entityUUID: UUID,
        action: 3,
        title: typeof name === "string" ? JSON.stringify({ "text": name }) : JSON.stringify(name),
        health: undefined,
        color: undefined,
        dividers: undefined,
        flags: undefined
    });
}

function deleteBossbar(vanillaclient) {
    vanillaclient.write('boss_bar', {
        entityUUID: UUID,
        action: 1,
        title: undefined,
        health: undefined,
        color: undefined,
        dividers: undefined,
        flags: undefined
    });
}

module.exports.inject = function (vanillaclient, client) {
    createBossbar(vanillaclient, client.utils.rainbowify("Maniaplay's proxy v2", true));
    vanillaclient.bossbar = function (name) {
        changeName(vanillaclient, name);
    };
};

module.exports.uninject = function (vanillaclient) {
    deleteBossbar(vanillaclient);
    vanillaclient.bossbar = undefined;
};