const fs = require("fs");
const bad = ["encrypt", "compress", "success"];
const vanillapackets = fs.readdirSync("./vanillapackets");
const clientpackets = fs.readdirSync("./clientpackets");


module.exports.inject = function (vanillaclient, client, server) {

    if (client.handlePacket) client.off("packet", client.handlePacket);
    if (vanillaclient.handlePacket) vanillaclient.off("packet", vanillaclient.handlePacket);
    client.handlePacket = function (data, meta) {
        if (bad.includes(meta.name)) return;
        if (clientpackets.includes(`${meta.name}.js`)) {
            let m = require(`../clientpackets/${meta.name}.js`);
            let res = m(vanillaclient, client, server, data);
            switch (res) {
                case undefined: break;
                case "cancel": return;
                default:
                    vanillaclient.write(meta.name, res);
                    return;
            }
        }
        vanillaclient.write(meta.name, data);
    };

    vanillaclient.handlePacket = function (data, meta) {
        if (bad.includes(meta.name)) return;
        if (vanillapackets.includes(`${meta.name}.js`)) {
            let m = require(`../vanillapackets/${meta.name}.js`);
            let res = m(vanillaclient, client, server, data);
            switch (res) {
                case undefined: break;
                case "cancel": return;
                default:
                    vanillaclient.write(meta.name, res);
                    return;
            }
        }
        client.write(meta.name, data);
    };


    client.on("packet", client.handlePacket);
    vanillaclient.on("packet", vanillaclient.handlePacket);
};

module.exports.uninject = function (vanillaclient, client) {
    if (client.handlePacket) client.off("packet", client.handlePacket);
    if (vanillaclient.handlePacket) vanillaclient.off("packet", vanillaclient.handlePacket);

    client.handlePacket = function (data, meta) {
        if (bad.includes(meta.name)) return;
        vanillaclient.write(meta.name, data);
    }; // probably want to still send packets
    vanillaclient.handlePacket = function (data, meta) {
        if (bad.includes(meta.name)) return;
        client.write(meta.name, data);
    }; // probably want to still send packets

    client.on("packet", client.handlePacket);
    vanillaclient.on("packet", vanillaclient.handlePacket);
};