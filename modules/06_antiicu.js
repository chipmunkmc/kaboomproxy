module.exports.inject = function (vanillaclient, client) {
    client.opStatus = true;
    if (!client.entityId) client.entityId = 0;
    client.icuDetector = [0, 0];

    if (client.entityId === 0) client.on("login", function (packet) {
        client.entityId = packet.entityId;
    });

    client.positionHandler = function (packet) {
        client.icuDetector[0] = packet.teleportId;
    };

    client.on("position", client.positionHandler);

    client.icuInterval = setInterval(function () {
        let difference = client.icuDetector[0] - client.icuDetector[1];
        client.icuDetector[1] = client.icuDetector[0];
        if (difference > 18 && difference <= 22) {
            client.utils.spawnBot(["/op @a", "/setblock ~ ~ ~ command_block{Command:\"sudo * icu stop\",auto:1b} destroy"]);
            setTimeout(function () {
                client.chat("/v on");
            }, 2000);

        }
    }, 1000);

    client.opHandler = function (packet) {
        if (client.entityId !== packet.entityId) return;
        switch (packet.entityStatus) {
            case 24:
                client.chat("/op @s[type=player]");
                break;
        }
    };

    client.on("entity_status", client.opHandler);
};

module.exports.uninject = function (vanillaclient, client) {
    clearInterval(client.icuInterval);
    client.icuInterval = undefined;
    client.opStatus = undefined;
    client.icuDetector = undefined;
    client.off("position", client.positionHandler);
    client.off("entity_status", client.opHandler);
    client.positionHandler = undefined;
    client.opHandler = undefined;
};
