const config = require("../config");


module.exports.inject = function (vanillaclient, client, server) {

    if (!client.loggedIn) client.on("login", function () {
        client.loggedIn = true;
        if (config.proxy.joinCommands.vanish) client.chat(`/essentials:vanish enable`);
        if (config.proxy.joinCommands.god) client.chat(`/essentials:god enable`);
        if (config.proxy.joinCommands.commandspy) client.chat(`/commandspy on`);
        if (config.proxy.joinCommands.skin) client.chat(`/extras:skin ${config.proxy.joinCommands.skin}`);
        if (config.proxy.joinCommands.nick) client.chat(`/nick ${config.proxy.joinCommands.nick}`);
        if (config.proxy.joinCommands.prefix) client.chat(`/extras:prefix ${config.proxy.joinCommands.prefix}`);
    });
};

module.exports.uninject = function (vanillaclient, client) {
    /* ... */
};