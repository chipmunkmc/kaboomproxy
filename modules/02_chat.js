module.exports.inject = function (vanillaclient, client) {
    client.chatQueue = [];
    client.chatInterval = setInterval(function () {
        if (client.chatQueue[0]) client.write("chat", { message: client.chatQueue[0] });
        client.chatQueue.shift();
    }, 400);
    client.chat = function () {
        let msg = [...arguments].join(" ");
        msg.match(/.{1,255}/g).forEach(function (cutMsg) {
            client.chatQueue.push(cutMsg);
        });
    };

    vanillaclient.chat = function () {
        vanillaclient.write("chat", {
            message: JSON.stringify({ text: [...arguments].join(" ") }),
            position: 1,
            sender: "00000000-0000-0000-0000-000000000000"
        });
    };
};

module.exports.uninject = function (vanillaclient, client) {
    vanillaclient.chat = undefined;
    client.chat = undefined;
    clearInterval(client.chatInterval);
    client.chatInterval = undefined;
    client.chatQueue = undefined;
};