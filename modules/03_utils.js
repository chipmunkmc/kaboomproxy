const mc = require("minecraft-protocol");
const config = require("../config");
const r = require("crypto").randomBytes;


function cmdBlock(client, cmd, depth = 3) {
    client.write("block_dig", { status: 0, location: { x: Math.floor(client.position.x), y: client.position.y - depth, z: Math.floor(client.position.z) }, face: 1 });
    client.write("held_item_slot", { slotId: 0 });
    client.write("set_creative_slot", {
        slot: 36,
        item: {
            present: true,
            itemId: 422,
            itemCount: 64,
            nbtData: undefined
        }
    });
    setTimeout(function () {
        client.write("block_place", {
            hand: 0,
            location: { x: Math.floor(client.position.x), y: client.position.y - depth, z: Math.floor(client.position.z) },
            direction: 1,
            cursorX: 0,
            cursorY: 0,
            cursorZ: 0,
            insideBlock: false
        });
    }, 50);
    setTimeout(function () {
        client.write("update_command_block", {
            "location": { x: Math.floor(client.position.x), y: client.position.y - depth, z: Math.floor(client.position.z) },
            "command": cmd,
            "mode": 0x01,
            "flags": 0x04
        });
    }, 100);
}

function spawnBot(commands, username) {
    try {
        let tempC = mc.createClient(Object.assign({ username: username || r(8).toString("hex") }, config.client));
        tempC.on("login", function () {
            commands.forEach(function (command, i) {
                setTimeout(function () {
                    tempC.write("chat", { message: command });
                }, i * 400);
            });
            setTimeout(function () {
                if (tempC.serializer) {
                    tempC.serializer.end();
                }
                tempC.socket.destroy();
                tempC = null;
            }, (commands.length * 400) + 100);
        });
        tempC.on('error', _ => { });
    } catch (e) { }
}

function componentToHex(c) {
    let hex = c.toString(16);
    return hex.length == 1 ? "0" + hex : hex;
}

function HSVtoHEX(h, s, v) {
    let r, g, b, i, f, p, q, t;
    i = Math.floor(h * 6);
    f = h * 6 - i;
    p = v * (1 - s);
    q = v * (1 - f * s);
    t = v * (1 - (1 - f) * s);
    switch (i % 6) {
        case 0:
            r = v;
            g = t;
            b = p;
            break;
        case 1:
            r = q;
            g = v;
            b = p;
            break;
        case 2:
            r = p;
            g = v;
            b = t;
            break;
        case 3:
            r = p;
            g = q;
            b = v;
            break;
        case 4:
            r = t;
            g = p;
            b = v;
            break;
        case 5:
            r = v;
            g = p;
            b = q;
            break;
    }

    r = Math.round(r * 255);
    g = Math.round(g * 255);
    b = Math.round(b * 255);
    return `${componentToHex(r)}${componentToHex(g)}${componentToHex(b)}`;
}


function rainbowify(text, returnJson) {
    let res = [];
    let hue = 0;
    for (char of text.split('')) {
        let hex = HSVtoHEX(hue, 1, 1);
        res.push({ text: char, color: `#${hex}` });
        hue += 1.0 / Math.max(text.length, 15);
    }
    if (returnJson) return res;
    return JSON.stringify(res);
}


module.exports.inject = function (vanillaclient, client) {
    client.utils = {};
    client.utils.cmdBlock = cmdBlock;
    client.utils.spawnBot = spawnBot;
    client.utils.rainbowify = rainbowify;
};

module.exports.uninject = function (vanillaclient, client) {
    client.utils = undefined;
    vanillaclient.utils = undefined;
};