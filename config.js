const config = {
    server: {
        host: "localhost",
        motd: "When the impostor isn't sus!",
        maxPlayers: 10,
        "online-mode": false
    },
    client: {
        host: "die.raccoon.pw",
        port: 25565
    },
    shared: {
        version: "1.16.5",
        keepAlive: false
    },
    proxy: {
        prefix: ".",
        hideCBSet: true,
        keys: {
            hbot: "hidden"
        },
        joinCommands: {
            vanish: true,
            god: true,
            commandspy: true,
            skin: "maniaplay", // set this to null if you dont want one
            nick: "&bma&dni&ra&dpl&bay", // set this to null if you dont want one
            prefix: "%" // set this to null if you dont want one
        },
        messages: {
            playerJoin: "§e{player} §ejoined the game", //set to null to disable
            playerLeave: "§e{player} §eleft the game" //set to null/false to disable
        }
    }
};

// don"t touch this.
Object.assign(config.server, config.shared);
Object.assign(config.client, config.shared);
module.exports = config;