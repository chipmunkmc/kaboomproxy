module.exports = function(vanillaclient, client, server, data) {
    client.position.x = data.x;
    client.position.y = data.y;
    client.position.z = data.z;
    client.position.yaw = data.yaw;
    client.position.pitch = data.pitch;
};