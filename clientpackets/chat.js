const config = require("../config");
module.exports = function (vanillaclient, client, server, data) {
    let msg = JSON.parse(data.message);
    if (msg.translate === "multiplayer.player.joined" && config.proxy.messages.playerJoin) {
        vanillaclient.chat(config.proxy.messages.playerJoin.replace(/\{player\}/g, msg.with[0].insertion));
        return "cancel";
    } else if (msg.translate === "multiplayer.player.joined" && !config.proxy.messages.playerJoin) return "cancel";
    if (msg.translate === "multiplayer.player.left" && config.proxy.messages.playerLeave) {
        vanillaclient.chat(config.proxy.messages.playerLeave.replace(/\{player\}/g, msg.with[0].text));
        return "cancel";
    } else if (msg.translate === "multiplayer.player.left" && !config.proxy.messages.playerLeave) return "cancel";
    if (msg.translate === "advMode.setCommand.success" && config.proxy.hideCBSet) return "cancel";


    if (msg.extra && msg.extra[0] && msg.extra[0].text === 'Vanish for '
        && msg.extra[msg.extra.length - 1] && msg.extra[msg.extra.length - 1].text === ': disabled'
        && config.proxy.joinCommands.vanish) {
        client.write('chat', { message: '/essentials:vanish enable' });
        setTimeout(()=>client.write('chat', { message: '/essentials:vanish enable' }),100);
        setTimeout(()=>client.write('chat', { message: '/essentials:vanish enable' }),400);
    }
};