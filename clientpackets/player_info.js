module.exports = function (vanillaclient, client, server, data) {
    data.data.forEach(function (player) {
        switch (data.action) {
            case 0:
                client.players[player.UUID] = player;
                client.players[player.UUID].vanish = false;
                break;
            case 1: // Update gamemode
                if (!client.players[player.UUID]) return;
                client.players[player.UUID].gamemode = player.gamemode;
                break;
            case 2: // Update latency
                if (!client.players[player.UUID]) return;
                client.players[player.UUID].ping = player.ping;
                break;
            case 3: // Update displayname, barely used
                if (!client.players[player.UUID]) return;
                client.players[player.UUID].displayName = player.displayName;
                break;
            case 4: // Remove player, also triggered while in vanish, need to check for "left the game" message
                if (!client.players[player.UUID]) return;
                client.players[player.UUID].vanish = true; // we assume it's vanish
                client.once("chat", function (data) { // capture a single message
                    if(!client.players[player.UUID]) return; // um
                    let translate = JSON.parse(data.message).translate;
                    let withthing = JSON.parse(data.message).with;
                    if(translate !== "multiplayer.player.left") return;
                    if(!withthing || !withthing[0]) return;
                    if (withthing[0].text === client.players[player.UUID].name) {
                        vanillaclient.write("player_info", {
                            action: 4,
                            data: [
                              {
                                UUID: player.UUID,
                                name: undefined,
                                properties: undefined,
                                gamemode: undefined,
                                ping: undefined,
                                displayName: undefined
                              }
                            ]
                          });
                        delete client.players[player.UUID];
                    }
                });
        }
    });
    if(data.action === 4) return "cancel";
};