const util = require("util");
module.exports.desc = "eval code";
module.exports.usage = "<code>";
module.exports.exec = function (vanillaclient, client, server, args) {
    try {
        /*jshint ignore:start*/
        vanillaclient.chat(util.inspect(eval(args.join(" "))));
        /*jshint ignore:end*/
    } catch (e) {
        vanillaclient.chat(`§4Error: ${e.toString().replace(/\n/g,"\n§4Error: ")}`);
    }
};