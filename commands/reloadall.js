const fs = require("fs");
module.exports.desc = "reloads everything. literally everything.";
module.exports.exec = function(vanillaclient, client, server, args) {

    vanillaclient.chat("Reloading...");
    let start = new Date();

    /* deinject all modules */
    for(let module of client.modules) {
        module.uninject(vanillaclient, client, server);
    }

    /* clear cache */
    for(let path of Object.keys(require.cache)) {
        if(path.includes("node_modules") || !path.includes(".js")) continue;
        delete require.cache[path];
    }

    /* inject all modules */
    client.modules = [];
    client.injectModules();

    let end = new Date();
    
    vanillaclient.chat(`Done! Took ${end - start}ms. Memory usage: ${process.memoryUsage().heapUsed/1000000}MB`);
};