module.exports.desc = "makes your text be rainbow (code from hhh, ported by me)";
module.exports.usage = "[--send] <text>";
module.exports.exec = function(vanillaclient, client, server, args) {
    let rainbow = client.utils.rainbowify(args.join(" ").replace("--send ",""));
    if(args[0] === "--send") return client.utils.cmdBlock(client, `/tellraw @a ${rainbow}`);
    vanillaclient.write("chat", {
        message: rainbow,
        position: 1,
        sender: "00000000-0000-0000-0000-000000000000"
    });
};