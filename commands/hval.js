const crypto = require("crypto");
const config = require("../config.js");
module.exports.desc = "automatically append hash for hbot commands";
module.exports.usage = "<hbot command>";
module.exports.exec = function(vanillaclient, client, server, args) {
    const sha256 = crypto.createHash("sha256");
    const command = `#${args.join(" ")}`;
    const time = Math.floor(+new Date() / 10000);
    let raw = command.replace(/&[0-9a-fklmnor]/g, "");
    raw += ";";
    raw += vanillaclient.username.replace(/§[0-9a-fklmnor]/g, "");
    raw += ";";
    raw += time;
    raw += ";";
    raw += config.proxy.keys.hbot;

    sha256.update(raw);
    const hash = sha256.digest();
    const big_int = hash.slice(0, 4).readUInt32BE();

    client.chat(`${command} ${big_int.toString(36)}`);
};