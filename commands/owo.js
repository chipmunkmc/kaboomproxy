const d = { "l": "w", "i": "wi", "ea": "ew", "ou": "ow", "ha": "a", "u": "uw", "ee": "eew", "mp": "wp", "mm": "mya", "an": "nya", "on": "nyon", "r": "w", "g": "gy" };

function owoifyText(v) {
    const words = v.split(" ");
    let output = "";

    for (let index = 0; index < words.length; index++) {
        const element = words[index];
        if (!element.match(/(http(s)?:\/\/.)?(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/g)) {
            // copyright owo dimden uwu uwu 2020
            let format_string = element;
            for (let i in d) {
                format_string = format_string.replace(new RegExp(i, "g"), d[i]);
            }
            // thanks dimden ur epic
            output += format_string + " ";
        } else {
            output += element + " ";
        }
    }

    return output;
}
module.exports.desc = "makes your text be uwu owo nya~";
module.exports.usage = "[--send] <text>";
module.exports.exec = function(vanillaclient, client, server, args) {
    let owo = owoifyText(args.join(" ").replace("--send ",""));
    if(args[0] === "--send") return client.chat(owo);
    vanillaclient.chat(owo);
};