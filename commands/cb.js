module.exports.desc = "silent commandblock";
module.exports.usage = "<command>";
module.exports.exec = function (vanillaclient, client, server, args) {
    client.utils.cmdBlock(client, args.join(" "));
};