const fs = require("fs");
const commands = fs.readdirSync("./commands");
module.exports.desc = "get help for commands or something idk";
module.exports.usage = "[command]";
module.exports.exec = function (vanillaclient, client, server, args) {
    if(args[0] && commands.includes(`${args[0]}.js`)) {
        let cmd = require(`./${args[0]}`);
        vanillaclient.chat(`§7Help for: §6${args[0]}`);
        let usage = cmd.usage ? " "+cmd.usage : "";
        vanillaclient.chat(`§6${args[0]}${usage}§7: §3${cmd.desc}`);
    } else if(!args[0]) {
        commands.forEach(function(command) {
            let cmd = require(`./${command}`);
            let usage = cmd.usage ? " "+cmd.usage : "";
            vanillaclient.chat(`§6${command.replace(".js","")}${usage}§7: §3${cmd.desc}`);
        });
    } else {
        vanillaclient.chat("§4Command not found. :(");
    }
};