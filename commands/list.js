module.exports.desc = "lists all the players in the server";
module.exports.usage = "[--send] [uuids]";
module.exports.exec = function (vanillaclient, client, server, args) {
    let players = "";
    for(let player of Object.values(client.players)) {
        players += `${player.name}§r`;
        if(args.join(" ").includes("uuids")) players += ` (${player.UUID})`;
        players += ", ";
    }
    let str = `There are ${Object.keys(client.players).length} players online: ${players.substr(0, players.length-2)}.`;
    if(!args.join(" ").includes("--send")) return vanillaclient.chat(str);
    client.utils.cmdBlock(client, `/bcraw ${str}`);
};