const config = require("./config");
const fs = require("fs");
const server = require("minecraft-protocol").createServer(config.server);

server.on("login", function (vanillaclient) {
    let client = require("minecraft-protocol").createClient(Object.assign({ username: vanillaclient.username }, config.client));
    client.modules = [];
    client.position = {};
    client.players = [];
    client.injectModules = function () {
        fs.readdirSync("./modules").forEach(function (module_file) {
            const module = require(`./modules/${module_file}`);
            module.inject(vanillaclient, client, server);
            client.modules.push(module);
        });
    };
//client.on('packet', (p,m)=>console.log(`${require('util').inspect(m.name)}, ${require('util').inspect(p)}`))
    client.injectModules();


    vanillaclient.on("end", () => client.end());
    vanillaclient.on("error", () => client.end());
    client.on("end", () => vanillaclient.end());
    client.on("error", () => vanillaclient.end());
});
